# Materialien für den MALIS-Studiengang

In diesem Repo plant und erstellt Adrian Pohl Lehr- und Lernmaterialien für den MALIS (Master in Library and Information Science) an der TH Köln.

## Das Lehr- und Lernmaterial

### Modul IT2

Die Lehre in diesem Modul teilen sich (im MALIS21 wie MALIS22) Adrian und Claudia Piesche, die die Inhalte zur digitalen Langzeitarchivierung und Langzeitverfügbarkeit übernimmt.

### Lehrsemester

#### MALIS24 IT2 (WS24/25)

- Präsenzzeiten, 10 Stunden:
  - Dienstag, 8.10. 13:15-16:30 Uhr (4 Unterrichtsstunden ; 15 min Pause inklusive) ([Slides](https://malis.acka47.net/it2/malis24/slides/2024-10-08.html))
  - Freitag, 6.12.  14-15:30 Uhr (2 UStd.) ([Slides](https://malis.acka47.net/it2/malis24/slides/2024-12-06.html))
  - Mittwoch, 22.1.24  13:15-16:30 Uhr (4 UStd. ; 15 min Pause inklusive) ([Slides](https://malis.acka47.net/it2/malis24/slides/2025-01-22.html))
- Aufgaben
  - Aufgabe IT2.1: Entweder [IT2.1a "Erstellung eines JSON-Schemas zur Validierung vorgegebener Beispieldaten"](https://codeberg.org/acka47/malis-it2.1a) ODER [IT2.1b "Konzeption einer Prozessoptimierung"](https://codeberg.org/acka47/malis/src/branch/main/pages/aufgabe2.1b.md)
  - Aufgabe IT2.2: Wird von Claudia Piesche gestellt
- Kleines IT2-Praxisprojekt: [Erstellung und Publikation eines SKOS-Vokabulars](https://codeberg.org/acka47/malis/src/branch/main/pages/praxisprojekt.md)

#### Vorherige Jahrgänge

Für vorherige Jahrgänge siehe die entsprechend getaggten Stände dieses Repos:

* [MALIS21](https://codeberg.org/acka47/malis/releases/tag/malis21)
* [MALIS22](https://codeberg.org/acka47/malis/releases/tag/malis22)
* [MALIS23](https://codeberg.org/acka47/malis/releases/tag/malis23)

### Zum lokalen Arbeiten an den Materialien

#### Slides lokal bauen

1. [reveal-md](https://github.com/webpro/reveal-md) installieren (auf neuester node-Version): `npm install -g reveal-md`
2. mit `reveal-md slides-md/` lokalen Server starten

#### Gleichzeitiges Arbeiten in `main` und `pages`

Die Slides werden mit reveal-md gebaut und über [Codeberg Pages](https://docs.codeberg.org/codeberg-pages/) veröffentlicht. Es hat sich gezeigt, dass es nützlich ist in beiden Branches (`main` und `pages`) gleichzeitig zu arbeiten, um zügig Slides neu bauen und veröffentlichen zu können:

- Dauerhaftes Auschecken des `pages`-Branch in einem eigenen Verzeichnis: `git worktree add ../malis-pages pages`.
- Bauen der aktuellen Slides: `$ reveal-md --static ~/git/malis-pages/it2/malis24/slides ~/git/malis/slides-md --theme white`
- Danach muss der Stand nur noch im `pages`-Branch committet und nach Codeberg in den `pages`-Branch gepusht werden.

#### PDF generieren

1. `$ nvm use 20`
2. `$ npm i -g decktape`
3. `$ decktape -s 1920x1080 reveal {link to slides} output.pdf`
