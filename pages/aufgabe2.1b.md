# Aufgabe IT2.1b des MALIS23: Konzeption einer Prozessoptimierung

![xkcd #518](https://imgs.xkcd.com/comics/flow_charts.png)

## Abgabefrist

15.12.2024

## Aufgabenstellung

- Vorstellung einen verbesserungsfähigen IT-unterstützten Prozess in der eigenen Einrichtung anhand eines Flussdiagramms (mittels Mermaid-Syntax) plus Erläuterungen
- Beschreibung der gewünschten Zielversion des Prozesses des Prozesses anhand eines weiteren Flussdiagramms
- Ergebnis: Markdown-Dokument mit Beschreibung des existierenden Prozesses und der erwünschten Lösung inklusive zweier mit Mermaid-Syntax erstellter Flussdiagramme

## Bewertungskriterien

Konsistente Darstellung im Flussdiagramm und verständliche Beschreibung der Problematik und Lösungsansätze im Fließtext mit Bezugnahme auf die Diagramme. Der Problemstellung und dem Lösungsansatz angemessen Analyse des Prozesses (nicht zu grob, nicht zu detailliert).

## Lernziele

Die Teilnehmer:innen können nach der Bearbeitung der Aufgabe:
* einen digital gestützen Prozess analysieren
* einen neu aufzusetzenden Prozess konzipieren
* Markdown-Dokumente mit grundlegender Textformatierung erstellen
* Flussdiagramme erstellen unter Nutzung der Mermaid-Syntax

## Orientierungshilfen

* Die Aufgabe sollte am besten im Markdown-Editor HedgeDoc zum kollaborativen Arbeiten und Präsentieren umgesetzt werden. Es gibt einige offene HedgeDoc-Instanzen, die das erstellen eines Pads ohne Login ermöglichen, z.B.:
    * https://hedgedoc.digillab.uni-augsburg.de/
    * https://hack.allmende.io/
    * https://md.darmstadt.ccc.de/
    * Bei Bedarf kann Adrian Pohl auf Anfrage auch ein Pad auf https://pad.lobid.org/ erstellen
* Eine Hilfe zur Markdown-Syntax öffnet sich beim Klick auf das Fragezeichen in der HedgeDoc-Navigationsleiste: ![](https://pad.gwdg.de/uploads/92fe5144-a82f-4e2e-bc68-e2a76ad5c96c.png)
* Eine Übersicht der vielfältigen HedgeDoc-Funktionen gibt es auf der jeweiligen Instanz unter `/features`, z.B. https://pad.lobid.org/features
* Markdown Style Guide: https://google.github.io/styleguide/docguide/style.html#document-layout
* Die Mermaid-Syntax für Flussdiagramme: http://mermaid.js.org/syntax/flowchart.html
* Mermaid Cheat Sheet: https://jojozhuang.github.io/tutorial/mermaid-cheat-sheet/#11-graph
* Beispieldiagramm: https://pad.gwdg.de/malis21-it2-aufgaben1?both
