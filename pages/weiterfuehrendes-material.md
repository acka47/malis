# Materialien zum Selbstlernen und zur Vertiefung

## HTTP

* MDN Web Docs, An overview of HTTP: https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview
* Julia Evans, HTTP: Learn your browser’s language!

## HTML
* MDN Web Docs, Verschiedene Lernressourcen zu HTML: https://developer.mozilla.org/en-US/docs/Web/HTML
* Erläuterungen zu den im Firefox eingebauten Tools (STRG+Umschalten+i): https://developer.mozilla.org/en-US/docs/Tools

## Kommandozeile

* Library Carpentry, The UNIX Shell: https://librarycarpentry.org/lc-shell/

## curl

Hier eine Übersicht von Julia Evans aus Bite Size Networking:

<img src="https://codeberg.org/acka47/malis/raw/branch/main/slides-md/img/curl-pmg-06.png" alt="Wichtige curl-Befehle aus Julia Evans' 'Bite Size Networking'" width="800"/>

* curl für Windows Einführung: https://www.ionos.de/digitalguide/server/tools/einstieg-in-curl-in-windows
* Everything curl – an extensive guide for all things curl: https://everything.curl.dev/ Hier insbesondere interessant: Networking simplified, HTTP basics, HTTP Redirects

## JSON Schema

* Understanding JSON Schema: http://json-schema.org/understanding-json-schema/

## RDF

* EasyRDF Converter zum überführen von RDF-Daten in unterschiedliche RDF-Visualisierungen: https://www.easyrdf.org/converter

## LOUD

* Pohl, Adrian / Steeg, Fabian / Christoph, Pascal (2018): lobid – Dateninfrastruktur für Bibliotheken. In: Informationspraxis 4(1). https://doi.org/10.11588/ip.2018.1.52445
* Steeg, Fabian / Pohl, Adrian / Christoph, Pascal (2019): lobid-gnd – Eine Schnittstelle zur Gemeinsamen Normdatei für Mensch und Maschine. In: Informationspraxis 5(1). https://doi.org/10.11588/ip.2019.1.52673
* [JSON-LD – Einführung und Anwendungen](https://slides.lobid.org/kim-ws-2018): Workshopfolien von 2018 inklusive einiger Übungen

## SPARQL & Wikidata

* Ivo Velitchkov (2020): Buckets and Balls. https://www.strategicstructures.com/?p=1889
* Wikidata Query Service in Brief
* Wikidata:SPARQL tutorial: https://www.wikidata.org/wiki/Wikidata:SPARQL_tutorial

## Wikidata

* Allison-Cassin, Stacy & Scott, Dan (2018): Wikidata: a platform for your library’s linked open data. In: Code4Lib Journal 40. URL: https://journal.code4lib.org/articles/13424
* Pohl, Adrian (2018): Was ist Wikidata und wie kann es die bibliothekarische Arbeit unterstützen? ABI Technik, 38 (2), S. 208. https://www.uebertext.org/2018/07/was-ist-wikidata-und-wie-kann-es-die.html
* Pohl, Adrian (2021): How We Built a Spatial Subject Classification Based on Wikidata. In: Code4Lib Journal 51. URL: https://journal.code4lib.org/articles/15875

## SKOS

* [Einführung in SKOS am Beispiel von Open Educational Resources (OER)](https://dini-ag-kim.github.io/skos-einfuehrung/)
* [Folien zu einem SKOS-Workshop von November 2022](https://pad.gwdg.de/p/2022-11-30-swib22-skos-workshop-slides)

## SkoHub Vocabs

* Romein, C. A. & Wagner, A. & van Zundert, J. J., (2023) “Building and Deploying a Classification Schema using Open Standards and Technology”, Journal for Digital Legal History 2(1). doi: https://doi.org/10.21825/dlh.85751
* [SkoHub-Slides des SKOS-Workshops von November 2022](https://pad.gwdg.de/p/2022-11-30-swib22-skos-workshop-slides#/51)
* How-To-Video “skohub-docker-vocabs forken und einrichten”: https://www.youtube.com/watch?v=JYVHiCYNl0U

## Suchmaschinentechnologie

* [lobid-gnd: Formulierung komplexer Suchanfragen](https://blog.lobid.org/2018/07/06/lobid-gnd-queries.html)